sub1=['101','102','103','104','105']
sub2=['201','202','203','204','205']
sub3=['301','302','303','304','305']
sub4=['401','402','403','404','405']

d={
1:{'Rollno1':{'name':'A','dob':'01-01-1995',
'sub':sub1,
'percentage':(None)},

'Rollno2':{'name':'B','dob':'02-02-1995',
'sub':sub1,
'percentage':(None)},

'Rollno3':{'name':'C','dob':'03-03-1995',
'sub':sub1,
'percentage':(None)}
},
2:{'Rollno1':{'name':'D','dob':'04-04-1994',
'sub':sub2,
'percentage':('50%')},

'Rollno2':{'name':'E','dob':'05-05-1994',
'sub':sub2,
'percentage':('70%')},

'Rollno3':{'name':'F','dob':'06-06-1994',
'sub':sub2,
'percentage':('65%')}
},
3:{'Rollno1':{'name':'G','dob':'07-07-1993',
'sub':sub3,
'percentage':('55%','57%')},

'Rollno2':{'name':'H','dob':'08-08-1993',
'sub':sub3,
'percentage':('70%','74%')},

'Rollno3':{'name':'I','dob':'09-09-1993',
'sub':sub3,
'percentage':('68%','60%')}
},
4:{'Rollno1':{'name':'J','dob':'10-10-1992',
'sub':sub4,
'percentage':('63%','55%','70%')},

'Rollno2':{'name':'K','dob':'11-11-1992',
'sub':sub4,
'percentage':('59%','64%','77%')},

'Rollno3':{'name':'L','dob':'12-12-1992',
'sub':sub4,
'percentage':('70%','80%','90%')}
}
}

for k, v in d.items():
    print k , "->" 
    print v.keys() ,"->"
    print v.values()
raw_input()